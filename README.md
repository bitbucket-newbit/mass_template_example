# Demo-Case

## Slow using builtin template:
Run Playbook:
```bash
ansible-playbook template_with_builtin_template.yml
```
## Optimized using mass_template_plugin (locally):
Run Playbook:
```bash
ansible-playbook template_with_mass_template.yml
```
## Optimized using mass_template_plugin (from collection):
Install newbit.tools collection containing action plugin:
```bash
ansible-galaxy collection install git@bitbucket.org:bitbucket-newbit/newbit.tools.git
```
Run Playbook:
```bash
ansible-playbook template_with_newbit_mass_template.yml
```
