#!/usr/bin/python
# Make coding more python3-ish, this is required for contributions to Ansible
#from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os.path
import tempfile

from ansible import constants as C
from ansible.errors import AnsibleActionFail
from ansible.module_utils._text import to_bytes, to_text, to_native
from ansible.plugins.action import ActionBase


class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        # Process arguments
        # Required
        src = self._task.args.get('src', None)
        remote_dest = self._task.args['remote_dest']
        output_var = self._task.args.get('output_var',None)
        loopvar = task_vars.get(self._task.args.get('loopvar', None),None)
        # Optional
        output_prefix = self._task.args.get('output_prefix','')
        output_extension = self._task.args.get('output_extension','')

        # Validate arguments
        if src is None or remote_dest is None or output_var is None:
            raise AnsibleActionFail("src, remote_dest and output_var are required")
        if not os.path.isfile(src):
            raise AnsibleActionFail(src + ' not found. Provide relative or absolute path to template')
        if loopvar is None:
            raise AnsibleActionFail("loopvar must exist in the runtime (try viewing it with debug)")

        # Add '.' to extension if not empty and not already supplied
        if output_extension != '' and not output_extension.startswith('.'):
            output_extension = '.' + output_extension

        # Create temporary directory 
        local_dest = tempfile.mkdtemp(dir=C.DEFAULT_LOCAL_TMP) + '/'

        # Initialize output list
        rendered_templates = []

        # Copy current 'facts' to modify them later
        temp_vars = task_vars.copy()
        
        # Read template
        with open(src, 'rb') as f:
            template_data = to_text(f.read(), errors='surrogate_or_strict')

        # Loop through loopvar and render templates
        for current_item in loopvar:
          # Set item 'fact' in temp_vars to use in template
          temp_vars['item'] = current_item

          # Build output filename
          out_file_name = local_dest + output_prefix + str(temp_vars['item'][output_var]) + output_extension

          # Render template with the modified 'facts'
          with self._templar.set_temporary_context(available_variables=temp_vars):
              rendered_template = self._templar.do_template(template_data, preserve_trailing_newlines=True, escape_backslashes=False)
          
          # Write the output
          with open(out_file_name, 'w+') as output_file:
              output_file.write(rendered_template)
              rendered_templates.append(out_file_name)

        # copy task args & set arguments for synchronize module
        sync_args = {
           "src": local_dest,
           "dest": remote_dest,
           "recursive": True,
           "archive": False,
           "checksum": True
        }

        # Execute synchronize module
        sync_out = self._execute_module(module_name='synchronize',
                             module_args=sync_args,
                             task_vars=task_vars, tmp=tmp)

        # Mark the task as 'changed' and return a list of rendered templates
        return dict(changed=sync_out['changed'], ansible_facts=dict(rendered_templates=rendered_templates, synced_files=sync_out['stdout_lines']))

