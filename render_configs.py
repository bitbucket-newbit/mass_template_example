"""Read group_vars/all/configs.yml and use values to render the config.ini.j2 template"""
import yaml # The group_vars file is yaml so we need yaml support
from jinja2 import Environment, FileSystemLoader # Environment will contain the templates and does the rendering, FileSystemLoader will lode the templates from disk

# Read group_vars
with open('group_vars/all/configs.yml') as config_file:
     configs = yaml.safe_load(config_file)

# Setup Jinja environment and define template path for FileSystemLoader
env = Environment(
    loader = FileSystemLoader('templates')
)

# Loop through all dicts 'values'
for current_config in configs['values']:
    # Load template
    config_template = env.get_template('config.ini.j2')
    # Open a filehandle with the target file name
    with open(f"/tmp/config{current_config['value']}.ini",'w') as config_output:
        # Put config in a dict with key "item" to call upon it with the standard Ansible loop var
        ansible_loopvar = { "item": current_config }
        # render template, supplying the current component variables
        rendered_template = config_template.render(ansible_loopvar)
        # Put it to disk
        config_output.write(rendered_template)
